# ether2html

Skeleton to design a webpage (for screen or print) collectively and synchronously using Etherpad, a collaborative text editor.

# How to use it

1. Create an etherpad somewhere (e.g. <https://framapad.org>).
2. Edit the file of this repository `ether2html.html` by replacing the URL you find on `line 18` by the URL of the pad you created in step 1.
3. Open the file `ether2html.html` in your web browser (Firefox or Chrome).
4. Edit your pad of step 1 with content (Markdown or not) and CSS styles. The page gets refreshed every minute. If you want to reload manually, delete the line 5 `<meta http-equiv="refresh" content="60" />`.


# Licence
© OSP under the GNU-GPL3