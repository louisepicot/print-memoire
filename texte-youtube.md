la tendance actuelle en développement

web et de vouloir séparer complètement

d'abord la partie cliente du site

c'est à dire les fichiers html css et

javascript qui sont interprétés par

navigateur de la partie serveur du site

c'est à dire les fichiers php si vous

développez votre site en php des

fichiers java si vous utilisez déjà bas

etc qui sont interprétés le côté serveur

c'est dans cette partie que l'on fait

des requêtes aux bases de données par

exemple

un site web au sens traditionnel du

terme et donc une application server qui

envoient des pages html dans le

navigateur du client à chaque fois que

celui ci le demande

quand l'utilisateur navigue sur votre

site et change de page il faut faire une

requête au serveur quand l'utilisateur

remplie il soumet un formulaire il faut

faire également une requête aux serveurs

etc bref tout cela est long et coûteux

et pourraient être faits plus rapidement

grâce à javascript

une application web est différente et

correspond à ce besoin

il s'agit d'une simple page html qui

contient suffisamment de javascript pour

pouvoir fonctionner en autonomie

une fois que le serveur l'a envoyé au

client

ici le schéma de gauche représente un

site web à chaque fois que l'utilisateur

demande une page le serveur s'occupe de

la renvoyer

slash accueil slash forums etc

dans le cas d'une application web le

serveur de renvoi qu'une seule page pour

l'ensemble du site

puis le javascript prend le relais pour

gérer la navigation en affichant ou

masquant des éléments html nécessaire

pour donner l'impression à l'internaute

qu'il navigue sur un site traditionnel

l' avantage de développer un site de

cette façon c'est qu'il est

incroyablement plus rapide

imaginez vous remplacez le délai d'une

requête aux serveurs par un traitement

javascript de plus comme vous n'avez pas

à recharger tout la page lors de la

navigation

vous pouvez permettre à l'utilisateur de

naviguer sur notre site tout en

discutant avec ses amis par exemple

comme sur la version web de facebook sur

le schéma de droite vous remarqué que

l'ensemble de l'application web est

contenue dans une seule page on appelle

ce genre d'architecturé une architecture

spa ce qui signifie simplement single

paige appliquée et cher

nous terminerons la première partie de

secours avec le traditionnel

hello world et nous validerons les

premières connaissances que vous aurez à

prises avec un questionnaire